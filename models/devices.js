var   sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (module_id, room_id, port, port_type, name, type, commands, callback) {
  db.run("INSERT INTO devices ( module_id, room_id, port, port_type, name, type, commands ) " +
          "VALUES ( ?, ?, ?, ?, ?, ?, ? );",
          [ module_id, room_id, port, port_type, name, type, commands ],
          function(err) {
              if (err) {
                callback(err);
              } else {
                callback(null, this.lastID);
              }
          });
}

module.exports.update = function (id, module_id, room_id, port, port_type, name, type, commands, callback) {
  db.run("UPDATE devices SET module_id= ?, room_id= ?, port= ?, port_type= ?, name= ?, type= ?, commands= ? " +
          "WHERE id= ?;",
          [ module_id, room_id, port, port_type, name, type, commands, id ],
          function(err) {
              if (err) {
                callback(err);
              } else {
                callback();
              }
          });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM devices;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM devices WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM devices WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}

module.exports.byModuleId = function (module_id, callback) {
  db.get("SELECT * FROM devices WHERE module_id= ? ;",
        [ module_id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}

module.exports.byRoomId = function (room_id, callback) {
  db.all("SELECT * FROM devices WHERE room_id= ? ;",
        [ room_id ],
        function(err, rows) {
            if (err) {
              callback(err);
            } else {
              if(rows){
                callback(null, rows);
              }else{
                callback(null, null);
              }

            }
        });
}
