var   crypto = require('bcrypt'),
      sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (name, icon, room_id, commands, callback ) {
          db.run("INSERT INTO scenes ( name, icon, room_id, commands ) " +
                  "VALUES ( ?, ?, ?, ? );",
                  [ name, icon, room_id, commands ],
                  function(err) {
                      if (err) {
                        callback(err);
                      } else {
                        callback();
                      }
                  });
}

module.exports.update = function (id, name, icon, callback) {
  db.run("UPDATE scenes SET name = ?, icon = ?, room_id= ?, commands= ? WHERE id= ?;",
        [ name, icon, room_id, commands, id ],
        function(err) {
            if (err) callback(err);
            else     callback();
        });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM scenes;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM scenes WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM scenes WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}
