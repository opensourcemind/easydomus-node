var   crypto = require('bcrypt'),
      sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (name, username, password, scene, event, config, callback) {

  //Create password hash
  crypto.genSalt(10, function(err, salt) {
      crypto.hash(password, salt, function(err, hash) {
          // Store hash in your password DB.
          db.run("INSERT INTO users ( name, username, password, scene, event, config) " +
                  "VALUES ( ?, ? , ?, ?, ?, ? );",
                  [ name, username, hash, scene, event, config ],
                  function(err) {
                      if (err) {
                        callback(err);
                      } else {
                        callback();
                      }
                  });
      });
  });
}

module.exports.update = function (id, name, username, scene, event, config, callback) {
  db.run("UPDATE users SET name = ?, username = ?, scene = ?, event = ?, config = ?  WHERE id= ?;",
        [ name, username, scene, event, config, id ],
        function(err) {
            if (err) callback(err);
            else     callback();
        });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM users;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM users WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM users WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}

module.exports.byUsername = function (username, callback) {
  db.get("SELECT * FROM users WHERE username= ? ;",
        [ username ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}
