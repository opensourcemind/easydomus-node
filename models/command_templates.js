var   sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (name, module_type, command_type, min_value, max_value, callback ) {
          db.run("INSERT INTO command_templates ( name, module_type, command_type, min_value, max_value ) " +
                  "VALUES ( ?, ?, ?, ?, ? );",
                  [ name, icon ],
                  function(err) {
                      if (err) {
                        callback(err);
                      } else {
                        callback();
                      }
                  });
}

module.exports.update = function (id, name, module_type, command_type, min_value, max_value, callback) {
  db.run("UPDATE command_templates SET name= ?, module_type= ?, command_type= ?, min_value= ?, max_value= ? WHERE id= ?;",
        [ name, module_type, command_type, min_value, max_value, id ],
        function(err) {
            if (err) callback(err);
            else     callback();
        });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM command_templates;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM command_templates WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM command_templates WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}
