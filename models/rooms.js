var   crypto = require('bcrypt'),
      sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (name, icon, callback ) {
          db.run("INSERT INTO rooms ( name, icon ) " +
                  "VALUES ( ?, ? );",
                  [ name, icon ],
                  function(err) {
                      if (err) {
                        callback(err);
                      } else {
                        callback();
                      }
                  });
}

module.exports.update = function (id, name, icon, callback) {
  db.run("UPDATE rooms SET name = ?, icon = ? WHERE id= ?;",
        [ name, icon, id ],
        function(err) {
            if (err) callback(err);
            else     callback();
        });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM rooms;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM rooms WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM rooms WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}
