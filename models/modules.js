var   sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

module.exports.create = function (name, address, network, channel, type, out_ports, in_ports,
                                  tag, version, callback) {
  db.run("INSERT INTO modules ( name, address, network, channel, type, out_ports, in_ports, tag, version) " +
          "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?);",
          [ name, address, network, channel, type, out_ports, in_ports, tag, version ],
          function(err) {
              if (err) {
                callback(err);
              } else {
                callback();
              }
          });
}

module.exports.update = function (id, name, address, network, channel, type, out_ports, in_ports,
                                  tag, version, callback) {
  db.run("UPDATE modules SET name= ?, address= ?, network= ?, channel= ?, type= ?, out_ports= ?, in_ports= ?, tag= ?, version= ? " +
          "WHERE id= ?;",
          [ name, address, network, channel, type, out_ports, in_ports, tag, version, id ],
          function(err) {
              if (err) {
                callback(err);
              } else {
                callback();
              }
          });
}

module.exports.getAll = function (callback) {
  db.all("SELECT * FROM modules;",
        function(err, rows) {
            if (err) callback(err);
            else     callback(null, rows);

            return;
        });
}

module.exports.delete = function (id, callback) {
  db.get("DELETE FROM modules WHERE id= ? ;",
        [ id ],
        function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
        });
}

module.exports.getById = function (id, callback) {
  db.get("SELECT * FROM modules WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}

module.exports.getByAddress = function (address, callback) {
  db.get("SELECT * FROM modules WHERE address= ? ;",
        [ modulename ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                callback(null, row);
              }else{
                callback(null, null);
              }

            }
        });
}

module.exports.updatePorts = function (id, device_id, port_type, port, name, callback) {
  db.get("SELECT * FROM modules WHERE id= ? ;",
        [ id ],
        function(err, row) {
            if (err) {
              callback(err);
            } else {
              if(row){
                var doUpdate = false;
                switch(port_type){
                  case 'out':
                      //output ports ->
                      var ports = JSON.parse(row.out_ports);
                      for(var i = 0; i < ports.length; i++){
                        if(ports[i].port_id == port){
                          ports[i].device_name = name;
                          ports[i].device_id = device_id;
                        }
                      }
                      out_ports = JSON.stringify(ports);
                      in_ports = row.in_ports;
                      doUpdate = true;
                    break;
                  case 'in':
                      //input ports <-
                      var ports = JSON.parse(row.in_ports);
                      for(var i = 0; i < ports.length; i++){
                        if(ports[i].port_id == port){
                          ports[i].device_name = name;
                          ports[i].device_id = device_id;
                        }
                      }
                      in_ports = JSON.stringify(ports);
                      out_ports = row.out_ports;
                      doUpdate = true;
                    break;
                }

                if(doUpdate){
                  db.run("UPDATE modules SET out_ports= ?, in_ports= ? " +
                        "WHERE id= ?;",
                        [ out_ports, in_ports, id ],
                        function(err) {
                            if (err) {
                              callback(err);
                            } else {
                              callback();
                            }
                        });
                }else{
                  callback(null);
                }
              }else{
                callback(null);
              }

            }
        });
}
