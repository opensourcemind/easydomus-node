var fs      = require('fs'),
    crypto  = require('bcrypt'),
    jwt     = require('jsonwebtoken'),
    sqlite3 = require('sqlite3').verbose(),
    users   = require('../models/users'),
    modules = require('../models/modules'),
    devices = require('../models/devices'),
    rooms   = require('../models/rooms'),
    scenes   = require('../models/scenes'),
    command_templates  = require('../models/command_templates'),
    easyon       = require('../controllers/easyon'),
    easylevel    = require('../controllers/easylevel'),
    easycontrol  = require('../controllers/easycontrol'),
    easysensor   = require('../controllers/easysensor'),
    easytest   = require('../controllers/easytest');

module.exports = function(app)
{

    var jsonError = { error: 500,  message: 'Método não implementado' };

     app.get('/',function(req,res){
        res.status(401).json({error: 401, message: 'Médoto não autorizado'});
     });

     app.get('/api',function(req,res){
        res.status(401).json({error: 401, message: 'Médoto não autorizado'});
     });

     app.get('/api/doc', function(req, res) {
       if (process.env.EDBRAIN_DEBUG) console.log(req);

       var path = __dirname + '/doc/apidoc.html';
       var file = fs.readFile(path, 'utf8', function(err, data) {
         if(err) {
           console.log(err);
         }
         res.send(data.toString());
       });

     });

     app.get('/api/utilities/test', function(req, res){
       if (process.env.EDBRAIN_DEBUG) console.log(req);

       var d = req.body;
       var pl = easytest.testFrame();
       res.json({
         error: 0,
         message: '',
         payload: pl.toString('hex').replace(/(.{2})/g, '$1 ').trim()
       });
     });

    //  app.post('/api/utilities/convertHex', function(req, res){
    //    var d = req.body;
    //    var r = 'easycontrol.sendIR( d.id, d.command);'
    //    res.json({
    //      error: 0,
    //      message: '',
    //      payload: r
    //    });
    //  });

     /**
      * Unauthenticated Utility APIs
      */
      app.post('/api/utilities/brain/pair', function(req, res){
        if (process.env.EDBRAIN_DEBUG) console.log(req);

        var d = req.body;
        var auth = true;

        if(auth) {
          // create a token
          var token = jwt.sign('admin', process.env.EDBRAIN_TOKEN, {
            expiresInMinutes: 1440 // expires in 24 hours
          });

          // return the information including token as JSON
          res.json({
            error: 0,
            message: 'Pareado com Easydomus Mind n/s: ' + d.address,
            token: token
          });
        }
      });

    /**
     * User APIs
     */

    app.post('/api/user/authenticate', function(req, res){
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      var d = req.body;
      users.byUsername(d.username, function(err, user){
        if(err || user == null){
          return res.status(404).json({ error: 401 , message: 'Acesso negado', token: ''});
        }else{
          console.log(user);

          //check hash

          crypto.compare(d.password, user.password, function(err, auth) {
              if(err){
                res.json({
                  error: 401,
                  message: 'Acesso negado',
                  token: ''
                });
              }

              // res == true
              if(auth) {
                // create a token
                var token = jwt.sign(d.username, process.env.EDBRAIN_TOKEN, {
                  expiresInMinutes: 1440 // expires in 24 hours
                });

                // return the information including token as JSON
                res.json({
                  error: 0,
                  message: 'Acesso autorizado',
                  user: user.name,
                  token: token
                });
              }else{
                // return the information including token as JSON
                res.json({
                  error: 401,
                  message: 'Acesso negado',
                  token: ''
                });
              }
          });
        }
      });
    });

    // route middleware to verify a token
    app.use(function(req, res, next) {
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      // check header or url parameters or post parameters for token
      var token = req.body.token || req.query.token || req.headers['x-access-token'];

      // decode token
      if (token) {

        // verifies secret and checks exp
        jwt.verify(token, process.env.EDBRAIN_TOKEN, function(err, decoded) {
          if (err) {
            return res.status(403).json({ error: 403 , message: 'Falha de autenticação de token.' });
          } else {
            // if everything is good, save to request for use in other routes
            req.decoded = decoded;
            next();
          }
        });

      } else {

        // if there is no token
        // return an error
        return res.status(403).json({ error: 403 , message: 'Nenhum token disponível.' });
      }
    });

    app.post('/api/user/create', function(req, res){
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      var d = req.body;
      users.create(d.name, d.username, d.password, d.scene, d.event, d.config, function(err){
        if(err) {
          return res.status(500).json({ error: 500 , message: 'Não foi possível criar o usuário', details: err });
        }else{
          return res.json({ error: 0 , message: 'Usuário criado' });
        }
      });
    });

    app.post('/api/user/update/', function(req, res){
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      var d = req.body;
      users.update(d.id, d.name, d.username, d.scene, d.event, d.config, function(err){
        if(err) {
          return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar o usuário id: ' + d.id, details: err });
        }else{
          return res.json({ error: 0 , message: 'Usuário id : ' + d.id + ' atualizado' });
        }
      });
    });

    app.get('/api/users', function(req, res){
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      users.getAll( function(err, rows){
        if(err || rows == null){
          return res.status(400).json({ error: 400 , message: 'Não foi possível listar os usuários', details: err});
        }else{
          return res.json({ error: 0 , message: '', users: rows });
        }

      });
    });

    app.get('/api/user/:id', function(req, res){
      if (process.env.EDBRAIN_DEBUG) console.log(req);

      users.getById( req.params.id, function(err, row){
        if(err || row == null){
          return res.status(404).json({ error: 404 , message: 'Usuário não encontrado', details: err});
        }else{
          return res.json({ error: 0 , message: '', user: row });
        }
      });
    });

    app.get('/api/user/delete/:id', function(req, res){
      users.delete( req.params.id, function(err){
            if(err){
              return res.status(500).json({ error: 500 , message: 'não foi possivel apagar o usuário', details: err});
            }else{
              return res.json({ error: 0 , message: 'Usuário ' + req.params.id + ' foi apagado.'});
            }
        });
    });


     /**
      * Macro APIs
      */
      app.post('/api/macro/create', function(req, res){ res.status(500).json(jsonError); });
      app.post('/api/macro/update', function(req, res){ res.status(500).json(jsonError); });
      app.get('/api/macros', function(req, res){ res.status(500).json(jsonError); });
      app.get('/api/macro/:id', function(req, res){ res.status(500).json(jsonError); });
      app.delete('/api/macro/:id', function(req, res){ res.status(500).json(jsonError); });

       app.post('/api/module/create', function(req, res){
         if (process.env.EDBRAIN_DEBUG) console.log(req);

         var d = req.body;
         var out_ports = [];
         var in_ports = [];

         for(i = 0; i < d.out_ports; i++){
           out_ports.push({ port_id: i, device_id: -1, device_name: "" });
         }

         for(i = 0; i < d.in_ports; i++){
           in_ports.push({ port_id: i, device_id: -1, device_name: "" });
         }

         out_ports = JSON.stringify(out_ports);
         in_ports = JSON.stringify(in_ports);

         modules.create(d.name, d.address, d.network, d.channel, d.type, out_ports, in_ports, d.tag, d.version, function(err){
           if(err) {
             return res.status(500).json({ error: 500 , message: 'Não foi possível adicionar o módulo', details: err });
           }else{
             return res.json({ error: 0 , message: 'Módulo adicionado' });
           }
         });
       });

       app.post('/api/module/update', function(req, res){
         if (process.env.EDBRAIN_DEBUG) console.log(req);

         var d = req.body;
         var out_ports = JSON.stringify(d.out_ports);
         var in_ports = JSON.stringify(d.in_ports);

         modules.update(d.id, d.name, d.address, d.network, d.channel, d.type, out_ports, in_ports, d.tag, d.version, function(err){
           if(err) {
             return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar o módulo', details: err });
           }else{
             return res.json({ error: 0 , message: 'Módulo atualizado.' });
           }
         });
       });

       app.get('/api/modules', function(req, res){
         if (process.env.EDBRAIN_DEBUG) console.log(req);

         modules.getAll( function(err, rows){
           if(err || rows == null){
             return res.status(400).json({ error: 400 , message: 'Não foi possível listar os módulos', details: err});
           }else{
             rows.forEach(function(row){
               var in_ports = JSON.parse(row.in_ports);
               var out_ports = JSON.parse(row.out_ports);

               row.in_ports = in_ports;
               row.out_ports = out_ports;
             });
             return res.json({ error: 0 , message: '', modules: rows });
           }

         });
       });

       app.get('/api/module/:id', function(req, res){
         if (process.env.EDBRAIN_DEBUG) console.log(req);

         modules.getById( req.params.id, function(err, row){
           if(err || row == null){
             return res.status(404).json({ error: 404 , message: 'Módulo não encontrado', details: err});
           }else{
             var in_ports = JSON.parse(row.in_ports);
             var out_ports = JSON.parse(row.out_ports);

             row.in_ports = in_ports;
             row.out_ports = out_ports;
             return res.json({ error: 0 , message: '', module: row });
           }
         });
       });

       app.get('/api/module/delete/:id', function(req, res){
         if (process.env.EDBRAIN_DEBUG) console.log(req);

         modules.delete( req.params.id, function(err){
               if(err){
                 return res.status(500).json({ error: 500 , message: 'não foi possivel apagar o módulo', details: err});
               }else{
                 return res.json({ error: 0 , message: 'Módulo ' + req.params.id + ' foi apagado.'});
               }
           });
       });


       /**
        * Device APIs
        */
        app.post('/api/device/create', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          var d = req.body;
          var commands = JSON.stringify(d.commands);

          devices.create(d.module_id, d.room_id, d.port, d.port_type, d.name, d.type, commands, function(err, lastID){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível adicionar o dispositivo', details: err });
            }else{
              var d = req.body;
              modules.updatePorts(d.module_id, lastID, d.port_type, d.port, d.name, function(err){
                if(err) {
                  return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar a porta', details: err });
                }else{
                  return res.json({ error: 0 , message: 'Dispositivo adicionado' });
                }
              });
            }
          });
        });

        app.post('/api/device/update', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          var d = req.body;
          var commands = JSON.stringify(d.commands);
          devices.update(d.id, d.module_id, d.room_id, d.port, d.port_type, d.name, d.type, commands, function(err){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar o dispositivo', details: err });
            }else{
              return res.json({ error: 0 , message: 'Dispositivo atualizado' });
            }
          });
        });

        app.get('/api/devices', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          devices.getAll( function(err, rows){
            if(err || rows == null){
              return res.status(400).json({ error: 400 , message: 'Não foi possível listar os dispositivos', details: err});
            }else{

              rows.forEach(function(row){
                var commands = JSON.parse(row.commands);
                row.commands = commands;
              });
              return res.json({ error: 0 , message: '', devices: rows });
            }

          });
        });

        app.get('/api/device/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          devices.getById( req.params.id, function(err, row){
            if(err || row == null){
              return res.status(404).json({ error: 404 , message: 'Dispositivo não encontrado', details: err});
            }else{
              var commands = JSON.parse(row.commands);
              row.commands = commands;
              return res.json({ error: 0 , message: '', device: row });
            }
          });
        });

        app.get('/api/device/delete/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          devices.delete( req.params.id, function(err){
                if(err){
                  return res.status(500).json({ error: 500 , message: 'Não foi possível apagar o dispositivo', details: err});
                }else{
                  return res.json({ error: 0 , message: 'Dispositivo ' + req.params.id + ' foi apagado'});
                }
            });
        });


        /**
         * Room APIs
         */
        app.post('/api/room/create', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          var d = req.body;
          rooms.create(d.name, d.icon, function(err){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível adicionar o ambiente', details: err });
            }else{
              return res.json({ error: 0 , message: 'Ambiente adicionado' });
            }
          });
        });

        app.post('/api/room/update', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          var d = req.body;
          rooms.update(d.id, d.name, d.icon, function(err){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar o ambiente', details: err });
            }else{
              return res.json({ error: 0 , message: 'Ambiente atualizado.' });
            }
          });
        });

        app.get('/api/rooms', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          rooms.getAll( function(err, rows){
            if(err || rows == null){
              return res.status(400).json({ error: 400 , message: 'Não foi possível listar os ambientes', details: err});
            }else{
              return res.json({ error: 0 , message: '', rooms: rows });
            }

          });
        });

        app.get('/api/room/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          rooms.getById( req.params.id, function(err, room){
            if(err || room == null){
              return res.status(404).json({ error: 404 , message: 'Ambiente não encontrado', details: err});
            }else{
              devices.byRoomId( room.id, function(err, devs){
                if(err || devs == null){
                  return res.status(404).json({ error: 404 , message: 'Dispositivos não encontrados neste ambiente', details: err});
                }else{
                    devs.forEach(function(dev){
                      var commands = JSON.parse(dev.commands);
                      dev.commands = commands;
                    });
                    room.devices = devs;
                  return res.json({ error: 0 , message: '', room: room });
                }
              });
            }
          });
        });

        app.get('/api/room/delete/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          rooms.delete( req.params.id, function(err){
                if(err){
                  return res.status(500).json({ error: 500 , message: 'não foi possivel apagar o ambiente', details: err});
                }else{
                  return res.json({ error: 0 , message: 'Ambiente ' + req.params.id + ' foi apagado.'});
                }
            });
        });


        /**
         * Scene APIs
         */
        app.post('/api/scene/create', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          var d = req.body;
          var commands = JSON.stringify(d.commands);
          scenes.create(d.name, d.icon, d.room_id, commands, function(err){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível adicionar o cenário', details: err });
            }else{
              return res.json({ error: 0 , message: 'Cenário adicionado' });
            }
          });
        });

        app.post('/api/scene/update', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);
          var d = req.body;
          var commands = JSON.stringify(d.commands);
          scenes.update(d.id, d.name, d.icon, d.room_id, d.commands, function(err){
            if(err) {
              return res.status(500).json({ error: 500 , message: 'Não foi possível atualizar o cenário', details: err });
            }else{
              return res.json({ error: 0 , message: 'Cenário atualizado.' });
            }
          });
        });

        app.get('/api/scenes', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          scenes.getAll( function(err, rows){
            if(err || rows == null){
              return res.status(400).json({ error: 400 , message: 'Não foi possível listar os cenários', details: err});
            }else{
              rows.forEach(function(row){
                var commands = JSON.parse(row.commands);
                row.commands = commands;
              });
              return res.json({ error: 0 , message: '', scenes: rows });
            }

          });
        });

        app.get('/api/scene/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          scenes.getById( req.params.id, function(err, scene){
            if(err || scene == null){
              return res.status(404).json({ error: 404 , message: 'Cenário não encontrado', details: err});
            }else{
                var commands = JSON.parse(dev.commands);
                scene.commands = commands;
                return res.json({ error: 0 , message: '', scene: scene });
            }
          });
        });

        app.get('/api/scene/delete/:id', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          scenes.delete( req.params.id, function(err){
                if(err){
                  return res.status(500).json({ error: 500 , message: 'não foi possivel apagar o cenário', details: err});
                }else{
                  return res.json({ error: 0 , message: 'Cenário ' + req.params.id + ' foi apagado.'});
                }
            });
        });


        /**
         * Command Templates
         */

        app.get('/api/commandTemplates', function(req, res){
          if (process.env.EDBRAIN_DEBUG) console.log(req);

          command_templates.getAll( function(err, rows){
            if(err || rows == null){
              return res.status(400).json({ error: 400 , message: 'Não foi possível listar comandos', details: err});
            }else{
              return res.json({ error: 0 , message: '', commandTemplates: rows });
            }

          });
        });


      /**
       * utilities command processing
       */

      app.post('/api/utilities/runCommand/',function(req,res){
        if (process.env.EDBRAIN_DEBUG) console.log(req);

         var d = req.body;
         devices.getById(d.device_id, function(err, dev){
           if(err || dev == null){
             return res.status(404).json({ error: 404 , message: 'Dispositivo não encotrado', details: err});
           }else{
             modules.getById(dev.module_id, function(err, mod){
               if(err || mod == null){
                 return res.status(404).json({ error: 404 , message: 'Módulo não encotrado', details: err});
               }else{
                 var cmd = null;
                 switch(mod.type){
                   case 1: //EasyOn
                      cmd = easyon.sendCommand(mod.address, dev.port, d.command);
                   break;
                   case 2: //EasyLevel
                      cmd = easylevel.sendCommand(mod.address, dev.port, d.command);
                   break;
                   case 3: //EasySenor
                      // easysensor.sendCommand(mod.address, dev.port);
                   break;
                   case 4: //EasyControl

                    var command = d.command;
                      console.log('command_id: ' + d.command_id + ' command: ' + command);
                      switch(d.command_id){
                        case 4:
                          switch(command){
                            case '18':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 3D FF FF FF 08 00 00 03";
                              break;
                            case '19':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB DD FF FF FF 04 00 00 03";
                              break;
                            case '20':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 5D FF FF FF 0C 00 00 03";
                              break;
                            case '21':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 9D FF FF FF 02 00 00 03";
                              break;
                            case '22':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 1D FF FF FF 0A 00 00 03";
                              break;
                            case '23':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB ED FF FF FF 06 00 00 03";
                              break;
                            case '24':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 6D FF FF FF 0E 00 00 03";
                              break;
                            case '25':
                                command = "02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB AD FF FF FF 01 00 00 03";
                              break;
                          }
                        break;
                      }
                      console.log('command_id: ' + d.command_id + ' command: ' + command);
                      cmd = easycontrol.sendCommand(mod.address, command);
                   break;
                 }
                 return res.json({ error: 0 , message: 'Command sent', command: cmd.toString('hex').replace(/(.{2})/g, '$1 ').trim()});
              }
             });
           }
         });
      });

}
