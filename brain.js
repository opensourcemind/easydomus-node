var express   = require('express'),
  morgan      = require('morgan'),
  bodyParser  = require('body-parser'),
  helpers     = require('./helpers/ed_utils'),
  db          = require('./helpers/db'),
  app         = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

db.initialize();

helpers.setupEdSerial();

require('./api/router')(app);

app.listen(3279, function() {
  console.log('EasyDomus Brain Listening on port 3279...');
})
