var helpers = require('../helpers/ed_utils');

module.exports.sendCommand = function (address, port, command){

  switch(port){
    case 0:
      if(command == 1){
        //On
        cmd = 65;
        add1 = 144;
        add2 = 145;
      }else{
        //Off
        cmd = 81;
        add1 = 80;
        add2 = 156;
      }
    break;

    case 1:
      if(command == 1){
        //On
        cmd = 66;
        add1 = 96;
        add2 = 145;
      }else{
        //Off
        cmd = 81;
        add1 = 160;
        add2 = 156;
      }
    break;

    case 2:
      if(command == 1){
        //On
        cmd = 67;
        add1 = 240;
        add2 = 144;
      }else{
        //Off
        cmd = 83;
        add1 = 48;
        add2 = 157;
      }
    break;

    case 3:
      if(command == 1){
        //On
        cmd = 68;
        add1 = 192;
        add2 = 146;
      }else{
        //Off
        cmd = 84;
        add1 = 0;
        add2 = 159;
      }
    break;

    case 4:
      if(command == 1){
        //On
        cmd = 69;
        add1 = 80;
        add2 = 147;
      }else{
        //Off
        cmd = 85;
        add1 = 144;
        add2 = 158;
      }
    break;

    case 5:
      if(command == 1){
        //On
        cmd = 70;
        add1 = 160;
        add2 = 147;
      }else{
        //Off
        cmd = 86;
        add1 = 96;
        add2 = 158;
      }
    break;

    case 6:
      if(command == 1){
        //On
        cmd = 71;
        add1 = 48;
        add2 = 146;
      }else{
        //Off
        cmd = 87;
        add1 = 240;
        add2 = 159;
      }
    break;

    case 7:
      if(command == 1){
        //On
        cmd = 72;
        add1 = 192;
        add2 = 151;
      }else{
        //Off
        cmd = 88;
        add1 = 0;
        add2 = 154;
      }
    break;
  }

  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(cmd),
                helpers.packNumber(0),
                helpers.packNumber(add1),
                helpers.packNumber(add2),
                helpers.packNumber(3) ] );

  //Sending to Serial
  helpers.sendToXbee(address, payload);
  return payload;
}

// module.exports.setMultiple = function (mac, dimmers, isPercent){
//   //Set all dimmers to NOCHANGE
//
//   var dimmer0 = isPercent ? dimmers[0] * 2: dimmers[0];
//   var dimmer1 = isPercent ? dimmers[1] * 2: dimmers[1];
//   var dimmer2 = isPercent ? dimmers[2] * 2: dimmers[2];
//   var dimmer3 = isPercent ? dimmers[3] * 2: dimmers[3];
//
//   var payload = new Buffer ( [ helpers.packNumber(2),
//                 helpers.packNumber(16),
//                 helpers.packNumber(4),
//                 helpers.packNumber(dimmer0),
//                 helpers.packNumber(dimmer1),
//                 helpers.packNumber(dimmer2),
//                 helpers.packNumber(dimmer3),
//                 helpers.packNumber(0),
//                 helpers.packNumber(3) ] );
//
//   //Sending to Serial
//   helpers.sendToXbee(mac, payload);
//   return payload;
// }

module.exports.setStatusLed = function (mac, leds){
  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(48),
                helpers.packNumber(2),
                helpers.packNumber(led[0]),
                helpers.packNumber(led[1]),
                helpers.packNumber(0),
                helpers.packNumber(3) ] );

  //Sending to Serial
  helpers.sendToXbee(mac, payload);
  return payload;
}
