var helpers = require('../helpers/ed_utils');

module.exports.testFrame = function (){
  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(16),
                helpers.packNumber(4),
                helpers.packNumber(100),
                helpers.packNumber(200),
                helpers.packNumber(255),
                helpers.packNumber(255),
                helpers.packNumber(0),
                helpers.packNumber(3) ] );

  //Sending to Serial
  console.log('Payload: ' + payload.toString('hex').replace(/(.{2})/g, '$1 ').trim());

  return payload;
}

module.exports.convertHex = function (hex){

  var payload = helpers.unpackHex(hex);


  // var payload = new Buffer ( [ helpers.packNumber(2),
  //               helpers.packNumber(16),
  //               helpers.packNumber(4),
  //               helpers.packNumber(100),
  //               helpers.packNumber(200),
  //               helpers.packNumber(255),
  //               helpers.packNumber(255),
  //               helpers.packNumber(0),
  //               helpers.packNumber(3) ] );

  //Sending to Serial
  console.log('Payload: ' + payload.toString('hex').replace(/(.{2})/g, '$1 ').trim());

  return payload;
}
