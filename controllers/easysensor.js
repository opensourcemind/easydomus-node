var helpers = require('../helpers/ed_utils');

module.exports.setSingle = function (mac, id, value, isPercent){
  //Set all dimmers to NOCHANGE
  var dimmers = [255,255,255,255];
  //Update the value for the single dimmer we
  //want to change
  dimmers[id]= isPercent ? value * 2: value;

  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(16),
                helpers.packNumber(4),
                helpers.packNumber(dimmers[0]),
                helpers.packNumber(dimmers[1]),
                helpers.packNumber(dimmers[2]),
                helpers.packNumber(dimmers[3]),
                helpers.packNumber(0),
                helpers.packNumber(3) ] );

  //Sending to Serial
  helpers.sendToXbee(mac, payload);
  return payload;
}

module.exports.setMultiple = function (mac, dimmers, isPercent){
  //Set all dimmers to NOCHANGE

  var dimmer0 = isPercent ? dimmers[0] * 2: dimmers[0];
  var dimmer1 = isPercent ? dimmers[1] * 2: dimmers[1];
  var dimmer2 = isPercent ? dimmers[2] * 2: dimmers[2];
  var dimmer3 = isPercent ? dimmers[3] * 2: dimmers[3];

  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(16),
                helpers.packNumber(4),
                helpers.packNumber(dimmer0),
                helpers.packNumber(dimmer1),
                helpers.packNumber(dimmer2),
                helpers.packNumber(dimmer3),
                helpers.packNumber(0),
                helpers.packNumber(3) ] );

  //Sending to Serial
  helpers.sendToXbee(mac, payload);
  return payload;
}

module.exports.setStatusLed = function (mac, leds){
  var payload = new Buffer ( [ helpers.packNumber(2),
                helpers.packNumber(48),
                helpers.packNumber(2),
                helpers.packNumber(led[0]),
                helpers.packNumber(led[1]),
                helpers.packNumber(0),
                helpers.packNumber(3) ] );

  //Sending to Serial
  helpers.sendToXbee(mac, payload);
  return payload;
}
