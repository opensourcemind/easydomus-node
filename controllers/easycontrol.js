var helpers = require('../helpers/ed_utils');

module.exports.sendCommand = function (mac, command){
  //Set all dimmers to NOCHANGE
  var commands = command.split(" ");
  var r = [];

  for(var i = 0; i < commands.length; i ++){
    r.push(helpers.packNumber(parseInt(commands[i], 16)));
  }

  var payload = new Buffer ( r );

  //Sending to Serial
  helpers.sendToXbee(mac, payload);
  return payload.toString('hex').replace(/(.{2})/g, '$1 ').trim();
}
