#!/bin/bash
rm -rf ../master.tar.gz
apt-get update
apt-get install build-essential
curl -sLS https://apt.adafruit.com/add | sudo bash
apt-get install node
apt-get install git
apt-get install sqlite3

npm install -g pm2 --unsafe-perm --production --loglevel warn
tar -xvzf node_modules.tgz
npm rebuild
#npm install --unsafe-perm --production --loglevel warn
pm2 startup ed.json
pm2 start ed.json
