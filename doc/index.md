# POST /api/user/create

+ Request (application/json)

    + Headers

            x-access-token: eyJhbGciOiJIUzI1NiJ9.dGVzdHVzZXI.R6RprPWBWmsAu-aohRdr3JA3g8r1-PfcRK_SPz0Z72g

    + Body

            {
                "name": "John Doe",
                "username": "jdoe4485",
                "password": "090909",
                "level": "100"
            }

+ Response 200 (application/json; charset=utf-8)

    + Headers

    + Body

            {"error":0,"message":"User Created."}


# POST /api/user/update

+ Request (application/json)

    + Headers

            x-access-token: eyJhbGciOiJIUzI1NiJ9.dGVzdHVzZXI.R6RprPWBWmsAu-aohRdr3JA3g8r1-PfcRK_SPz0Z72g

    + Body

            {
                "name": "Johnny Doe5",
                "username": "jdoe5",
                "password": "abcdef",
                "level": 50,
                "id": 5
            }

+ Response 200 (application/json; charset=utf-8)

    + Headers

    + Body

            {"error":0,"message":"User id: 5 updated"}

# GET /api/users

+ Request

    + Headers

            x-access-token: eyJhbGciOiJIUzI1NiJ9.dGVzdHVzZXI.R6RprPWBWmsAu-aohRdr3JA3g8r1-PfcRK_SPz0Z72g

+ Response 200 (application/json; charset=utf-8)

    + Headers

    + Body

            {"error":0,"message":"","users":[{"id":1,"name":"John Doe","username":"jdoe4485","password":"$2a$10$swqAmX/eukmjbMp3i.D/Tez/6yv23Kl4WW3946BKN5tJen0QLTLnq","level":100}]}

# POST /api/user/authenticate

+ Request (application/json)

        {
            "username": "jdoe4485",
            "password": "090909"
        }

+ Response 200 (application/json; charset=utf-8)

    + Headers

    + Body

            {"error":0,"message":"Access granted","token":"eyJhbGciOiJIUzI1NiJ9.amRvZTQ0ODU.M2UPa2esPFxr_QtN45qD_W5hksBdA2pOKPPKRQNuOHc"}
