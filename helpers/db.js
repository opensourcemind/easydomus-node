var   sqlite3 = require('sqlite3').verbose(),
      db = new sqlite3.Database('./database/easydomus.db');

 module.exports.initialize = function () {
     // Initilize Users Table
     db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='users'",
            function(err, rows) {
       if(err !== null) {
         console.log(err);
         process.exit(1);
       } else if(rows === undefined) {
          db.run('CREATE TABLE "users" ' +
                '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                '"name" VARCHAR(255), ' +
                '"username" VARCHAR(50), ' +
                '"password" VARCHAR(64), ' +
                '"scene" INTEGER, ' +
                '"event" INTEGER, ' +
                '"config" INTEGER ) ', function(err) {
                    if(err !== null) {
                      console.log(err);
                      process.exit(1);
                    } else {
                      console.log("SQL Table 'users' initialized.");
                    }
         });
       } else {
         console.log("SQL Table 'users' already initialized.");
       }
     });

     // Initilize Rooms Table
     db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='rooms'",
            function(err, rows) {
       if(err !== null) {
         console.log(err);
         process.exit(1);
       } else if(rows === undefined) {
          db.run('CREATE TABLE "rooms" ' +
                '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                '"name" VARCHAR(255), ' +
                '"icon" VARCHAR(255) )', function(err) {
                    if(err !== null) {
                      console.log(err);
                      process.exit(1);
                    } else {
                      console.log("SQL Table 'rooms' initialized.");
                    }
         });
       } else {
         console.log("SQL Table 'rooms' already initialized.");
       }
     });

     // Initilize Scenes Table
     db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='scenes'",
            function(err, rows) {
       if(err !== null) {
         console.log(err);
         process.exit(1);
       } else if(rows === undefined) {
          db.run('CREATE TABLE "scenes" ' +
                '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                '"name" VARCHAR(255), ' +
                '"icon" VARCHAR(255), ' +
                '"room_id" INTEGER, ' +
                '"commands" TEXT )', function(err) {
                    if(err !== null) {
                      console.log(err);
                      process.exit(1);
                    } else {
                      console.log("SQL Table 'scenes' initialized.");
                    }
         });
       } else {
         console.log("SQL Table 'scenes' already initialized.");
       }
     });

     // Initilize modules Table
     db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='modules'",
            function(err, rows) {
       if(err !== null) {
         console.log(err);
         process.exit(1);
       } else if(rows === undefined) {
          db.run('CREATE TABLE "modules" ' +
                '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                '"name" VARCHAR(255), ' +
                '"address" VARCHAR(64), ' +
                '"network" INTEGER, ' +
                '"channel" INTEGER, ' +
                '"type" VARCAHR(2), ' +
                '"out_ports" TEXT, ' +
                '"in_ports" TEXT, ' +
                '"tag" VARCHAR(20), ' + //Type may be something like easyon, easeylevel, easysensor, easycontrol
                '"version" VARCHAR(10)) '
                , function(err) {
                    if(err !== null) {
                      console.log(err);
                      process.exit(1);
                    } else {
                      console.log("SQL Table 'modules' initialized.");
                    }
         });
       } else {
         console.log("SQL Table 'modules' already initialized.");
       }
     });

     // Initilize devices Table
     db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='devices'",
            function(err, rows) {
       if(err !== null) {
         console.log(err);
         process.exit(1);
       } else if(rows === undefined) {
          db.run('CREATE TABLE "devices" ' +
                '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                '"room_id" INTEGER, ' +
                '"module_id" INTEGER, ' +
                '"port" INTEGER, ' +
                '"port_type" VARCHAR(32), ' +
                '"name" VARCHAR(255), ' +
                '"type" VARCHAR(32), ' + //Type may be something like easyon, easeylevel, easysensor, easycontrol
                '"commands" TEXT ) '
                , function(err) {
                    if(err !== null) {
                      console.log(err);
                      process.exit(1);
                    } else {
                      console.log("SQL Table 'devices' initialized.");
                    }
         });
       } else {
         console.log("SQL Table 'devices' already initialized.");
       }
     });

       // Initilize Command Templates Table
       db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='command_templates'",
              function(err, rows) {
         if(err !== null) {
           console.log(err);
           process.exit(1);
         } else if(rows === undefined) {
            db.run('CREATE TABLE "command_templates" ' +
                  '("id" INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                  '"name" VARCHAR(255), ' +
                  '"make" VARCHAR(255), ' +
                  '"model" VARCHAR(255), ' +
                  '"module_type" INTEGER, ' +
                  '"command_type" VARCHAR(20), ' +
                  '"min_value" VARCHAR(255), ' +
                  '"max_value" VARCHAR(255)); '
                  , function(err) {
                      if(err !== null) {
                        console.log(err);
                        process.exit(1);
                      } else {
                        console.log("SQL Table 'command_templates' initialized.");
                        db.run('INSERT INTO `command_templates` ' +
                              '(id,name,make,model,module_type,command_type,min_value,max_value) ' +
                              'VALUES (1,"Interruptor","Easy Domus","Easy On",1,"switch","0","1"), ' +
                              '(2,"Dimmer","Easy Domus","Easy Level",2,"dimmer","0","100"), ' +
                              '(3,"Power","AC","AC123",4,"infra-red","02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 EB 1D FF FF FF 0A 00 00 03","02 20 1A 01 00 00 84 03 C2 01 39 00 39 00 AA 00 00 02 00 00 2D 00 06 FB 1D FF FF FF 0B 00 00 03"), ' +
                              '(4,"Temperatura","AC","AC123",4,"infra-red","18","25"), ' +
                              '(5,"Power","Sony","Projetor",4,"infra-red","02 20 16 01 01 00 F0 00 3C 00 3C 00 3C 00 78 00 00 02 00 00 0F 00 02 EA 55 00 00 03","02 20 16 01 01 00 F0 00 3C 00 3C 00 3C 00 78 00 00 02 00 00 0F 00 02 EA 55 00 00 03"), ' +
                              '(6,"Power","Samsung","LCD TV",4,"infra-red","02 20 19 01 08 00 C1 01 BE 01 3B 00 A2 00 3B 00 23 12 00 01 21 00 05 07 07 02 FD 00 00 00 03","02 20 19 01 08 00 C1 01 BE 01 3B 00 A2 00 3B 00 23 12 00 01 21 00 05 07 07 02 FD 00 00 00 03"), ' +
                              '(7,"Volume","Samsung","LCD TV",4,"infra-red","02 20 19 01 00 00 CC 01 BD 01 3E 00 A0 00 2F 00 C7 F6 00 01 21 00 05 07 07 0B F4 00 00 00 03","02 20 19 01 00 00 C6 01 C3 01 43 00 9E 00 43 00 31 01 00 01 21 00 05 07 07 07 F8 00 00 00 03"), ' +
                              '(8,"Canal","Samsung","LCD TV",4,"infra-red","02 20 19 01 00 00 CC 01 BD 01 3F 00 A1 00 2F 00 C7 F6 00 01 21 00 05 07 07 61 9E 00 00 00 03","02 20 19 01 00 00 CB 01 BD 01 41 00 A1 00 30 00 C7 F6 00 01 21 00 05 07 07 60 9F 00 00 00 03"), ' +
                              '(9,"Input","Samsung","LCD TV",4,"infra-red","02 20 19 01 00 00 C9 01 BB 01 3B 00 A0 00 2C 00 C7 F6 00 01 21 00 05 07 07 01 FE 00 00 00 03","02 20 19 01 00 00 C9 01 BB 01 3B 00 A0 00 2C 00 C7 F6 00 01 21 00 05 07 07 01 FE 00 00 00 03") ;'
                              , function(err) {
                                  if(err !== null) {
                                    console.log(err);
                                    process.exit(1);
                                  } else {
                                    console.log("SQL Table 'command_templates' loaded.");
                                  }
                       });
                      }
           });

         } else {
           console.log("SQL Table 'command_templates' already initialized.");
         }
       });

  }
