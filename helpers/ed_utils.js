var util = require('util');
var SerialPort = require('serialport').SerialPort;
var xbee_api = require('xbee-api');

var C = xbee_api.constants;
var xbeeAPI = new xbee_api.XBeeAPI({
	api_mode: 1
});

var serialport = new SerialPort("/dev/ttyAMA0", {
		baudrate: 9600,
		parser: xbeeAPI.rawParser()
});

module.exports.packNumber = function (val) {
	var hex = parseInt(val,10).toString(16);
	var utf = "0x" + ("0" + hex).substr(-2);
	return utf
}

module.exports.packString = function (val) {
	var hex = val.charCodeAt(0).toString(16);
	var utf = "0x" + ("0" + hex).substr(-2);
	return utf
}

module.exports.unpackHex = function (hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}


module.exports.sendToXbee = function (mac, payload) {

		if(!serialport.isOpen()){
			console.log('Opening Serial Port...');
			serialport.open();
		}else{
			var frame_obj = {
						type: 0x10,
						id: 0x01,
						destination64: mac,
						broadcastRadius: 0x00,
						options: 0x00,
						data: payload
				};

			 	serialport.write(xbeeAPI.buildFrame(frame_obj), function(){
					serialport.drain( function (err) {
						if(!err){
							console.log('Sent ' + payload.toString('hex').replace(/(.{2})/g, '$1 ').trim() + ' to XBEE ' + mac + ' via Serial Port');
						}else{
							console.log('Error sending ' + payload.toString('hex').replace(/(.{2})/g, '$1 ').trim() + ' to XBEE ' + mac + ' via Serial Port');
						}
					});
				});
		}
}

module.exports.setupEdSerial = function (){
	//serial port closed... handling it
	serialport.on('close', function() {
	  console.console.log('Serial port closed...');
	  // serialport.open();
	});

	//error when dealing with serial port...
	serialport.on('error', function() {
	  console.log('Error opening/operating serial port, attemting to reopen...');
	  // serialport.open();
	});

	xbeeAPI.on("frame_object", function(frame) {
					//  console.log("Packet received from: " + frame.remote64 + ' with data: ' + frame.data);
					console.log(frame);
	 });

}
