# Easy Domus brain

In order to deploy EasyDomus Brain into your server or embedded device
you must have a running Debian (or other derivative) distribution and you must execute the following as root (sudo su):

```
wget https://bitbucket.org/opensourcemind/easydomus-node/get/master.tar.gz
tar -xvzf master.tar.gz
mv opensourcemind-easydomus-node* ed
cd ed
chmod +x deploy.sh
./deploy.sh
```

To check if you processes are running correctly you should run:
```
# pm2 list
```

You can check the logs by running :
```
# pm2 logs
```

### Reinstall/Update your installation:
 ***  THIS WILL DELETE YOUR ED FILES AND DATABASE ***
```
pm2 stop all
rm -rf master.tar.gz
rm -rf ed
wget https://bitbucket.org/opensourcemind/easydomus-node/get/master.tar.gz
tar -xvzf master.tar.gz
mv opensourcemind-easydomus-node* ed
cd ed
chmod +x deploy.sh
./deploy.sh
```

[C] Developed by Open Source Mind for Incorporato Technology
